#!/bin/bash

#This script loops over all the lines created by create-docker-list.sh and creates a directory for them depending on name of container,
#copies the contents of the logs to them and changes the ownership of all the files so that they can be SCP'ed back

while IFS='' read -r line || [[ -n "$line" ]]; do
     line=$(echo "${line#/}")
     now=$(date +"%m_%d_%Y")
     mkdir -p /tmp/logs/$line
     logFile=$(docker inspect --format='{{.LogPath}}' $line)
     cp $logFile /tmp/logs/$line/$line-$now
     cd /tmp/logs
     chown antunesm:antunesm $line
     cd /tmp/logs/$line
     chown antunesm:antunesm $line-$now
done < "$1"

chown antunesm:antunesm /tmp/logs
